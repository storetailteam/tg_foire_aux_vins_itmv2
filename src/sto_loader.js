var settings = require("./settings.json");
settings.btf_container_img = settings.btf_container_img === "" ? "none" : "url(./../../img/"+settings.btf_container_img+")";
settings.btf_couleur = settings.btf_couleur === "" ? "none" : settings.btf_couleur;
settings.buttons_couleur = settings.buttons_couleur === "" ? "none" : settings.buttons_couleur;
settings.buttons_couleur_select = settings.buttons_couleur_select === "" ? "none" : settings.buttons_couleur_select;
settings.buttons_img = settings.buttons_img === "" ? "none" : "url(./../../img/"+settings.buttons_img+")";
settings.buttons_img_select = settings.buttons_img_select === "" ? "none" : "url(./../../img/"+settings.buttons_img_select+")";
settings.buttons_txt = settings.buttons_txt === "" ? "none" : settings.buttons_txt;
settings.buttons_txt_select = settings.buttons_txt_select === "" ? "none" : settings.buttons_txt_select;
settings.download_img = settings.download_img === "" ? "none" : "url(./../../img/"+settings.download_img+")";
settings.border_color = settings.border_color === "" ? "none" : settings.border_color;
settings.buttons_width = settings.buttons_width === "" ? "0" : settings.buttons_width;
settings.logo_img = settings.logo_img === "" ? "none" : "url(./../../img/"+settings.logo_img+")";
settings.bg_format = settings.bg_format === "" ? "none" : "url(./../../img/"+settings.bg_format+")";
settings.bg_carrousel = settings.bg_carrousel === "" ? "none" : "url(./../../img/"+settings.bg_carrousel+")";
settings.bg_big_carrousel = settings.bg_big_carrousel === "" ? "none" : "url(./../../img/"+settings.bg_big_carrousel+")";
settings.design_left = settings.design_left === "" ? "none" : "url(./../../img/"+settings.design_left+")";
settings.design_right = settings.design_right === "" ? "none" : "url(./../../img/"+settings.design_right+")";
settings.button_select_arrow = settings.button_select_arrow === "" ? "none" : "url(./../../img/"+settings.button_select_arrow+")";
settings.arrow_pos_button_select = settings.arrow_pos_button_select === "" ? "0" : settings.arrow_pos_button_select;
settings.sprite_retailer = settings.sprite_retailer === "" ? "none" : "url(./../../img/"+settings.sprite_retailer+")";

module.exports = function (source) {
    this.cacheable();
    var test = source
        .replace(/__PLACEHOLDER__/g, settings.name)
        .replace(/__BTF_CONTAINE_IMG__/g,settings.btf_container_img)
        .replace(/__BTFCOLOR__/g,settings.btf_couleur)
        .replace(/__BUTTONCOULEUR__/g,settings.buttons_couleur)
        .replace(/__BUTTONCOULEURSELECT__/g,settings.buttons_couleur_select)
        .replace(/__BUTTONIMG__/g,settings.buttons_img)
        .replace(/__BUTTONIMGSELECT__/g,settings.buttons_img_select)
        .replace(/__BUTTONTXT__/g,settings.buttons_txt)
        .replace(/__BUTTONTXTSELECT__/g,settings.buttons_txt_select)
        .replace(/__BUTTONSELECTARROW__/g,settings.button_select_arrow)
        .replace(/__DLIMG__/g,settings.buttons_txt_select)
        .replace(/__BORDERCOLOR__/g,settings.border_color)
        .replace(/__LOGOIMG__/g,settings.logo_img)
        .replace(/__BUTTONSWIDTH__/g,settings.buttons_width)
        .replace(/__BGFORMAT__/g,settings.bg_format)
        .replace(/__BGCARROUSEL__/g,settings.bg_carrousel)
        .replace(/__BGBIGCARROUSEL__/g,settings.bg_big_carrousel)
        .replace(/__DESIGNLEFT__/g,settings.design_left)
        .replace(/__DESIGNRIGHT__/g,settings.design_right)
        .replace(/__DLIMG__/g,settings.download_img)
        .replace(/__APOSBTNSELECT__/g,settings.arrow_pos_button_select)
        .replace(/__SPRITERETAILER__/g,settings.sprite_retailer);
    return test;
};
