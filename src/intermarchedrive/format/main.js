"use strict";

var sto = window.__sto,
    settings = require("./../../settings.json"),
    $ = window.jQuery,
    html = require("./main.html"),
    placeholder = settings.name,
    style = require("./main.css"),
    container = $(html),
    format = settings.format,
    myurl = window.location.href,
    myurl = myurl.replace("https://", "").split("/")[1];

module.exports = {
    init: _init_(),
    styleGutter: style
}

var landing = document.location.toString().split('?')[1];

// SVG Regions

var regions = {
    "st1": ["Beaujolais","/boutique/2909"],
    "st4": ["Provence-et-Corse","/boutique/2794"],
    "st6": ["Jura-Savoie","/boutique/4344"],
    "st7": ["Alsace","/boutique/2792"],
    "st8": ["Champagne","/boutique/2898"],
    "st9": ["Bourgogne","/boutique/2793"],
    "st10": ["Languedoc-Roussillon","/boutique/2791"],
    "st11": ["Bordeaux","/boutique/4340"],
    "st12": ["Sud-Ouest","/boutique/2917"],
    "st13": ["Rhone","/boutique/2790"],
    "st14": ["Val-de-Loire","/boutique/2789"],
    "st15": ["Vins-etrangers","/boutique/4319"]
};

function _init_() {
    sto.load(format, function(tracker) {
        var container = $(html);
        tracker.display();
        tracker.sendHit({
            "ta": "view"
        });


        $('.js-menu_boutique > ul > li[data-tag="foire_aux_vins"]').click(function() {




        })

        style.use();
        $("#content .contenu").empty().prepend(container);

        var currentID,oldID = "";
        $('.sto-'+placeholder+'-center svg path, .sto-'+placeholder+'-rest_world div').not(".st5").hover(
          function(){
            currentID = $(this).attr('class');
            $('.sto-'+placeholder+'-center svg path').removeAttr('selected');
            $('.sto-'+placeholder+'-center svg path.'+currentID).attr('selected','');

            if(currentID != oldID && currentID != "st5"){
                tracker.sendHit({
                     "ta": "nav",
                     "tz": currentID,
                     "tv": regions[currentID][0]
                });
                $(".sto-"+placeholder+"-cta, .sto-"+placeholder+"-right").css({'display':'inline-block'}).attr('region',currentID);
            }
            oldID = currentID;
          } , function() {
            // $('.sto-'+placeholder+'-center svg path').removeAttr('selected');
            // $(".sto-"+placeholder+"-cta, .sto-"+placeholder+"-right").css({'display':'none'}).removeAttr('region');
            // oldID = '';

          });

          $('.sto-'+placeholder+'-center svg path.st5').mouseenter(function() {
              $('.sto-'+placeholder+'-center svg path').removeAttr('selected');
              $(".sto-"+placeholder+"-cta, .sto-"+placeholder+"-right").css({'display':'none'}).removeAttr('region');
              oldID = '';
          });

        // $('.sto-'+placeholder+'-cta').mouseenter(
        //   function(){
        //     var currentRegion = $(this).attr('region');
        //
        //     // $('.sto-'+placeholder+'-cta').removeAttr('selected');
        //     $('.sto-'+placeholder+'-center svg path.'+currentRegion).attr('selected','');
        //   });
        // $('.sto-'+placeholder+'-cta').mouseleave(
        //   function(){
        //     var currentRegion = $(this).attr('region');
        //     if ($('.sto-'+placeholder+'-center svg path.'+currentRegion).attr('selected') == 'selected' ) {
        //
        //     } else {
        //       $('.sto-'+placeholder+'-center svg path').removeAttr('selected');
        //       $(".sto-"+placeholder+"-cta, .sto-"+placeholder+"-right").css({'display':'none'}).removeAttr('region');
        //     }
        //   });









        // setLandingPage('Val-de-Loire');

        $(document).find('#rdf-cta').on('click', function() {
            var currentID = $(this).attr('region');
            tracker.sendHit({
                "ta": "clk",
                "tz": currentID,
                "tv": regions[currentID][0]
            });
            window.open(regions[currentID][1], "_self");
        });

        $(document).find('path, .sto-'+placeholder+'-rest_world div').on('click', function() {
            var currentID = $(this).attr('class');
            if(currentID && currentID != "st5" && currentID != ""){
                tracker.sendHit({
                    "ta": "clk",
                    "tz": currentID,
                    "tv": regions[currentID][0]
                });
                window.open(regions[currentID][1], "_self");
            }
        });


        $("html, body").animate({ scrollTop: 0 },0);

        var removers = {};
        removers[format] = function() {
            style.unuse();
            container.remove();
        };
        return removers;
    });
}


function setLandingPage(landing){
    var rg = '';
    switch(landing){
        case 'Beaujolais':rg='st1';break;
        case 'Provence-Corse':rg='st4';break;
        case 'Jura-Savoie':rg='st6';break;
        case 'Alsace':rg='st7';break;
        case 'Champagne':rg='st8';break;
        case 'Bourgogne':rg='st9';break;
        case 'Languedoc-Roussillon':rg='st10';break;
        case 'Bordeaux':rg='st11';break;
        case 'Sud-Ouest':rg='st12';break;
        case 'Rhone':rg='st13';break;
        case 'Val-de-Loire':rg='st14';break;
        case 'Vins-etrangers':rg='st15';break;
        default:break;
    }
    if(landing != '' && rg != ''){
        $('.sto-'+placeholder+'-center svg path').removeAttr('selected');
        $('.sto-'+placeholder+'-center svg path.'+rg).attr('selected','');
        $(".sto-"+placeholder+"-cta, .sto-"+placeholder+"-right").css({'display':'inline-block'}).attr('region',rg);
    }
}
